var WeatherStation = WeatherStation || {};

WeatherStation.Config = {

  ROOT_URL: CONFIG.ROOT_URL,
  ROOT_WEBSERVICE_URL: CONFIG.ROOT_WEBSERVICE_URL,
  IMAGE_WEBSERVICE_URL: CONFIG.IMAGE_WEBSERVICE_URL,
  HOME_URL: "/home/show"

};
