/**
 * Register Handlebars Helpers
 */
export function registerHelpers() {

  Handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
  });

  Handlebars.registerHelper('isDefined', function(arg1) {
    if (arg1 == null || arg1 == undefined || arg1 == "") {
      return false;
    }
    return true;
  });

  Handlebars.registerHelper('replaceNullWithDashes', function(arg1) {
    if (arg1 == null || arg1 == undefined || arg1 == "") {
      return "--";
    }
    return arg1;
  });

  Handlebars.registerHelper('formatZeroWithDecimal', function(arg1) {
    if (arg1 == "0") {
      return arg1 + ".0"
    }
    return arg1;
  });

  Handlebars.registerHelper('defaultNumberToZero', function(arg1) {

    if (arg1 == "0" || arg1 == null || arg1 == undefined) {
      return "0";
    }
    return arg1;
  });

  Handlebars.registerHelper('formatDateTime', function(bYear, arg1) {

    let oDateTime = WeatherStation.Util.parseInstantDateTime(arg1);
    let formattedDate = '';
    if(bYear == "true") {
      formattedDate = `${oDateTime.sMonth}-${oDateTime.day}-${oDateTime.year} ${WeatherStation.Util.padHour(oDateTime.hour)}:${WeatherStation.Util.appendLeadingZeroes(oDateTime.minute)}`;
    } else {
      formattedDate = `${oDateTime.sMonth}-${oDateTime.day} ${WeatherStation.Util.padHour(oDateTime.hour)}:${WeatherStation.Util.appendLeadingZeroes(oDateTime.minute)}`;
    }
    return formattedDate;
  });

  Handlebars.registerHelper('formatDateTimeStripTZ', function(sTimestamp) {
    sTimestamp = sTimestamp.replace('T', ' ');
    sTimestamp = sTimestamp.replace('Z', '');
    return sTimestamp;
  });

  Handlebars.registerHelper('ifgt', function(val1, val2, options) {
    if (val1 > val2) {
      return options.fn(this);
    }
    return options.inverse(this);
  });

  Handlebars.registerHelper('getStatusCodeColor', function(statusCode) {

    switch(statusCode) {
      case 100:
        return 'icon-green';
      case 200:
        return 'icon-red';
      case 300:
        return 'icon-yellow';
    }
  });
}

/**
 * Register Handlebars Partials
 */
export function registerPartials() {

  Handlebars.registerPartial('TABLE_PAGENATION', Handlebars.templates.Pagenation);
  Handlebars.registerPartial('TABLE_SORT_ICON', Handlebars.templates.TableSortIcon);

  Handlebars.registerPartial('STATION_DETAILS', Handlebars.templates.StationDetails);
  Handlebars.registerPartial('STATION_CURRENT_CONDITIONS', Handlebars.templates.StationCurrentConditions);

  Handlebars.registerPartial('IMAGE_SEARCH_PAGENATION', Handlebars.templates.ImagePagenation);
}
