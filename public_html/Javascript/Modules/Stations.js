export function getStationSummaries() {

  WeatherStation.DataService.getAllActiveStationSummaries().then(function(aStations) {

    if (!WeatherStation.Util.validateWebserviceCall(aStations, "station-list-holder")) return;
    if (!WeatherStation.Util.validateListLengthGreaterThanZero(aStations, new WeatherStation.Class.Message("No Stations Found."), "station-list-holder")) return;

    let oData = {};
    oData.stations = aStations;

    for (var i = 0; i < oData.stations.length; i++) {
      oData.stations[i].rootURL = WeatherStation.Config.ROOT_URL;
      try {
        oData.stations[i].viewSettings = JSON.parse(oData.stations[i].primarySite.viewSettings);
      } catch (e) {
        oData.stations[i].viewSettings = null;
      }
    }

    WeatherStation.Util.executeHandlebars("station-list-holder", "StationList", oData, "clear");
  });
}

export function getStations() {

  WeatherStation.DataService.getAllActiveStations().then(function(aStations) {

    if (!WeatherStation.Util.validateWebserviceCall(aStations, "station-list-holder")) return;
    if (!WeatherStation.Util.validateListLengthGreaterThanZero(aStations, new WeatherStation.Class.Message("No Stations Found."), "station-list-holder")) return;

    let oData = {};
    oData.stations = aStations;

    for (var i = 0; i < oData.stations.length; i++) {
      oData.stations[i].rootURL = WeatherStation.Config.ROOT_URL;
    }

    WeatherStation.Util.executeHandlebars("station-list-holder", "StationList", oData, "clear");
  });

}

export function getStationDetails(iStationId, iActiveSite) {

  WeatherStation.DataService.getStationDetails(iStationId).then(function(oStation) {

    if (!WeatherStation.Util.validateWebserviceCall(oStation)) return;

    if(oStation.station.viewSettings) {
      oStation.station.viewSettings = JSON.parse(oStation.station.viewSettings);
    }

    let oData = oStation;

    oData.rootURL = WeatherStation.Config.ROOT_URL;

    document.getElementById("station-name").innerHTML = oData.station.displayName;

    WeatherStation.Util.executeHandlebars("details-holder", "StationDetails", oData, "clear");
    generateSiteTabs(oStation, iActiveSite);
    generateNWSForecast(oStation);
  });
}

export function retryNWSForecast(sNwsOffice, iNwsGridX, iNwsGridY) {

    let oStation = {
      'station' : {
        'nwsOffice': sNwsOffice,
        'nwsGridX': iNwsGridX,
        'nwsGridY': iNwsGridY,
        'viewSettings': {
          'nws_forecast': true
        }
      }
    }
    generateNWSForecast(oStation);
}

async function generateNWSForecast(oStation) {

  if(oStation.station?.viewSettings?.nws_forecast) {

    let response = await fetch('https://api.weather.gov/gridpoints/'+oStation.station.nwsOffice+'/'+oStation.station.nwsGridX+','+oStation.station.nwsGridY+'/forecast');
    if (response.ok) { // if HTTP-status is 200-299
      // get the response body (the method explained below)
      let json = await response.json();

      let oData = json.properties;
      oData.office = oStation.station.nwsOffice;
      oData.gridX = oStation.station.nwsGridX;
      oData.gridY = oStation.station.nwsGridY;

      WeatherStation.Util.executeHandlebars("NWS-forecast-holder", "NWSForecast", oData, "clear");


    } else {

      let oError = {
        type: 'ERR',
        header: 'Error retrieving NWS forecast:',
        details: response.status,
        office: oStation.station.nwsOffice,
        gridX: oStation.station.nwsGridX,
        gridY: oStation.station.nwsGridY
      }
      WeatherStation.Util.executeHandlebars("NWS-forecast-holder", "NWSForecastError", oError, "clear");
    }

  }

}

function generateSiteTabs(oStation, iActiveSite) {

  let aSites = oStation.sites;

  let iIndexActive = null;

  if(iActiveSite == undefined || iActiveSite == null || iActiveSite == "") {
    iIndexActive = 0;
  }

  for(let i = 0; i < aSites.length; i++) {

    if(iIndexActive != null) {
      if(i == iIndexActive) aSites[i].activeTab = true;
    } else {
      if(aSites[i].id == iActiveSite)aSites[i].activeTab = true;
    }

    aSites[i].renderedViewSettings = JSON.parse(aSites[i].viewSettings);
  }

  let oData = {};
  oData.sites = aSites;
  oData.station = oStation.station;
  oData.rootURL = WeatherStation.Config.ROOT_URL;
  oStation.sites = aSites;

  WeatherStation.Util.executeHandlebars('site-tabs-holder', 'StationSiteTabs', oData, 'clear');
  getLatestSiteRecords(oStation);
  getTwelveHourRecords(oStation);

}

function getLatestSiteRecords(oStation) {

  let aSites = oStation.sites;

  for(let i = 0; i < aSites.length; i++) {

    WeatherStation.DataService.getLatestSiteRecord(oStation.station.id, aSites[i].id).then(function(oLatestRecord) {

      if (!WeatherStation.Util.validateWebserviceCall(oLatestRecord, `site-current-record-holder-${aSites[i].id}`)) return;

      var oData = {};
      oData.renderedViewSettings = aSites[i].renderedViewSettings;
      oData.record = oLatestRecord;

      WeatherStation.Util.executeHandlebars(`site-current-record-holder-${aSites[i].id}`, 'StationCurrentConditions', oData, 'clear');

    });

  }
}

function getTwelveHourRecords(oStation) {

  let iDt = getCurrentDateTimeInTimeZone(oStation.station.timezone);

  let dtTo = new Date(iDt);
  let dtFrom = new Date(iDt);

  dtFrom.setHours(dtFrom.getHours() - 12);

  let sDateFrom = WeatherStation.Util.formatDate(dtFrom, "YYMMDDHHmm");
  let sDateTo = WeatherStation.Util.formatDate(dtTo, "YYMMDDHHmm");

  let oPageableRequest = new WeatherStation.Class.PageableRequest();
  oPageableRequest.startDate = sDateFrom;
  oPageableRequest.endDate = sDateTo;
  oPageableRequest.source = oPageableRequest.dataSources.RECENT;
  oPageableRequest.page = 0;
  oPageableRequest.size = 500;
  oPageableRequest.sort = 'timestamp';
  oPageableRequest.sortDir = oPageableRequest.sortDirs.DESC;

  let aSites = oStation.sites;

  for(let i = 0; i < aSites.length; i++) {

    WeatherStation.DataService.getStationRecords(oStation.station.id, aSites[i].id, oPageableRequest).then(function(oPageableResponse) {

      if (!WeatherStation.Util.validateWebserviceCall(oPageableResponse, `twelvehr-table-holder-${aSites[i].id}`)) {
        WeatherStation.Util.validateWebserviceCall(oPageableResponse, `twelvehr-graph-holder-${aSites[i].id}`);
        return;
      }

      renderTwelveHourTable(aSites[i], oPageableResponse.content);

      let aCharts = [];
      WeatherStation.Charts.renderTwelveHourChart(aSites[i], oPageableResponse.content, aCharts);

    });

  }

  function getCurrentDateTimeInTimeZone(sTimezone) {
    return Intl.DateTimeFormat('en', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      timeZone: sTimezone,
      hour12: false,
    }).format(new Date());
  }

}

/**
 *
 * STATION DETAILS TABLE
 *
 */
function renderTwelveHourTable(oSite, aRecords) {

  var oRenderedViewSettings = oSite.renderedViewSettings.records;

  // Check if heat index or wind chill data is present. If not, do not show columns on table.
  let bDataExists = validateDataExists(aRecords);

  if (bDataExists.heatIndex && oRenderedViewSettings.heatIndex.enabled) {
    oRenderedViewSettings.heatIndex.enabled = true;
  } else {
    oRenderedViewSettings.heatIndex.enabled = false;
  }

  if (bDataExists.windChill && oRenderedViewSettings.windChill.enabled) {
    oRenderedViewSettings.windChill.enabled = true;
  } else {
    oRenderedViewSettings.windChill.enabled = false;
  }


  let oData = {};
  oData.records = aRecords;
  oData.renderedViewSettings = oRenderedViewSettings;

  WeatherStation.Util.executeHandlebars(`twelvehr-table-holder-${oSite.id}`, 'TwelveHourTable', oData, 'clear');


  function validateDataExists(aRecords) {

    let oExists = {
      heatIndex: false,
      windChill: false
    }

    for (let i = 0; i < aRecords.length; i++) {
      if (aRecords[i].heatIndex != null) oExists.heatIndex = true;
      if (aRecords[i].windChill != null) oExists.windChill = true;
    }

    return oExists;
  }
}

/*
 * CURRENT CONDITION TOOL BAR FUNCTIONALITY
 */
export function refreshCurrentConditions(iStationId, iSiteId) {

  WeatherStation.DataService.getStationDetails(iStationId).then(function(oStation) {

    if (!WeatherStation.Util.validateWebserviceCall(oStation)) return;

    let oSite = {};

    for(let i = 0; i < oStation.sites.length; i++) {
      if(oStation.sites[i].id == iSiteId) {
        oSite = oStation.sites[i];
        oSite.renderedViewSettings = JSON.parse(oStation.sites[i].viewSettings);
      }
    }

    oStation.sites = [];
    oStation.sites.push(oSite);

    getLatestSiteRecords(oStation);

  });
}

export function openTechnicalStatsModal(iStationId, iSiteId) {

  let oData = {};
  WeatherStation.DataService.getStationStatus(iStationId).then(function(oStatus) {

    if (!WeatherStation.Util.validateWebserviceCall(oStatus)) {
      oData.error = true;

    } else {
      oData.error = false;
      oData.status = oStatus;

      WeatherStation.Util.executeHandlebars('modal-content', 'TechnicalStats', oData, 'clear');
    }
  });
}
