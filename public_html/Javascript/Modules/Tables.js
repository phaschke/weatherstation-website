let _LOADED_SITE_VIEW_SETTINGS = {};
let _LOADED_RECORDS = [];

function setLoadedSiteViewSettings(oSiteViewSettings) {
  _LOADED_SITE_VIEW_SETTINGS = oSiteViewSettings;
}

function getLoadedSiteViewSettings() {
  return _LOADED_SITE_VIEW_SETTINGS;
}

function orderSiteSettings(oLoadedSiteViewSettings) {

  let aLoadedSiteViewKeys = Object.keys(oLoadedSiteViewSettings);
  aLoadedSiteViewKeys.forEach((key) => {
    oLoadedSiteViewSettings[key].key = key;
  });

  let aViewSettings = Object.values(oLoadedSiteViewSettings);

  oLoadedSiteViewSettings = aViewSettings.sort((a, b) => {
    return a.order - b.order;
  });

  return oLoadedSiteViewSettings;
}

/**
 *
 * SEARCHABLE TABLE
 *
 */
export function getStationDetails(iStationId, iSiteId) {

  WeatherStation.DataService.getStationDetails(iStationId).then(function(oStation) {

    if (!WeatherStation.Util.validateWebserviceCall(oStation)) return;

    for (let i = 0; i < oStation.sites.length; i++) {
      if (oStation.sites[i].id == iSiteId) {
        oStation.site = oStation.sites[i];
      }
    }

    document.getElementById("stationName").innerHTML = oStation.station.displayName;
    document.getElementById("siteName").innerHTML = oStation.site.title;

    let oSiteViewSettings = JSON.parse(oStation.site.viewSettings);
    setLoadedSiteViewSettings(oSiteViewSettings.records);

    renderSearchableTable(iStationId, iSiteId, null);
  });
}

function getRequestedDataAndRenderTable(iStationId, iSiteId, oPageableRequest) {

  let oSavedDataSourceSettings = getSavedDataSourceSettings(iSiteId);
  if (oSavedDataSourceSettings.recent && oSavedDataSourceSettings.truncated) {
    oPageableRequest.source = oPageableRequest.dataSources.BOTH;
  } else if (oSavedDataSourceSettings.recent) {
    oPageableRequest.source = oPageableRequest.dataSources.RECENT;
  } else if (oSavedDataSourceSettings.truncated) {
    oPageableRequest.source = oPageableRequest.dataSources.TRUNCATED;
  } else {
    renderSearchableTable(iStationId, iSiteId, {});
    return;
  }

  WeatherStation.DataService.getStationRecords(iStationId, iSiteId, oPageableRequest).then(function(oPageableResponse) {

    if (!WeatherStation.Util.validateWebserviceCall(oPageableResponse)) {
      return;
    }
    renderSearchableTable(iStationId, iSiteId, oPageableResponse, oPageableRequest.source);

  });
}

function renderSearchableTable(iStationId, iSiteId, oPageableResponse, sSource) {

  let oData = {};
  let oEnabledColumnSettings = getEnabledTableColumns(iSiteId);
  oData.enabledColumnSettings = oEnabledColumnSettings;
  oData.orderedColumnSettings = orderSiteSettings(oEnabledColumnSettings);

  if (oPageableResponse && oPageableResponse.content && oPageableResponse.content.length > 0) {

    oData.records = oPageableResponse.content;
    oData.stationId = iStationId;
    oData.siteId = iSiteId;

    if (sSource == 'BOTH') {
      oData.sortEnabled = false;
    } else {
      oData.sortEnabled = true;
    }
  }

  if (oPageableResponse && oPageableResponse.totalPages != undefined && oPageableResponse.totalElements != undefined) {
    let oPageableResponseClass = new WeatherStation.Class.PageableResponse(oPageableResponse);
    oData.pagenation = oPageableResponseClass.toDisplayJSON();
    oData.sortField = oPageableResponseClass.sortField;
    oData.sortDirection = oPageableResponseClass.sortDirection;
  }

  WeatherStation.Util.executeHandlebars('record-table-holder', 'TableSearchable', oData, 'clear');

}

export function onSearch(iStationId, iSiteId, iPage, sSort, sSortDir) {

  let oDates = WeatherStation.Util.validateDateFields();
  if (!oDates) return false;

  let oSortDetails = getSortColumn(iSiteId);
  if (sSort == undefined || sSort == null || sSort == "") {
    if (oSortDetails.sort != undefined && oSortDetails.sort != "") {
      sSort = oSortDetails.sort;
    } else {
      sSort = 'timestamp';
    }
  }
  if (sSortDir == undefined || sSortDir == null || sSortDir == "") {
    if (oSortDetails.sortDir != undefined && oSortDetails.sortDir != "") {
      sSortDir = oSortDetails.sortDir;
    } else {
      sSortDir = 'DESC';
    }
  }

  if (iPage == undefined || iPage == null) {
    iPage = 0;
  }

  let oData = {};
  oData.loading = true;
  WeatherStation.Util.executeHandlebars('record-table-holder', 'TableSearchable', oData, 'clear');

  //TODO: Replace with stored values
  //let sSort = undefined;
  //let sSortDir = undefined;

  let oPageableRequest = new WeatherStation.Class.PageableRequest();
  oPageableRequest.startDate = oDates.startDate;
  oPageableRequest.endDate = oDates.endDate;
  oPageableRequest.page = iPage;
  oPageableRequest.size = 50;
  oPageableRequest.sort = sSort;
  oPageableRequest.sortDir = oPageableRequest.sortDirs[sSortDir];

  getRequestedDataAndRenderTable(iStationId, iSiteId, oPageableRequest);
}

export function onColumnClick(iStationId, iSiteId, sKey, sDirection) {

  if (sDirection == 'DESC') {
    sDirection = 'ASC';
  } else {
    sDirection = 'DESC';
  }

  onSearch(iStationId, iSiteId, 0, sKey, sDirection);
}

function getSortColumn(iSiteId) {

  let oSavedDataSourceSettings = getSavedDataSourceSettings(iSiteId);

  // Only allow timestamp desc when both data sources are used
  if (oSavedDataSourceSettings.recent && oSavedDataSourceSettings.truncated) {
    return {
      sort: 'timestamp',
      sortDir: 'DESC'
    };
  }

  let aSortElement = document.getElementsByClassName('sortedField');

  if (aSortElement.length <= 0) {
    return {
      sort: undefined,
      sortDir: undefined
    };
  }

  let eSortElement = aSortElement.item(0)
  let sSortId = eSortElement.id;
  let aSortIdArr = sSortId.split('-');

  return {
    sort: aSortIdArr[1],
    sortDir: aSortIdArr[2]
  };
}

/**
 *
 * COLUMN VISIBILITY
 *
 */
function getAllSavedColumnVisibility() {

  let sSavedColumnVisibility = WeatherStation.LocalStorage.getData("SearchableTableColumnVisibility");

  try {
    let oSavedColumnVisibility = JSON.parse(sSavedColumnVisibility);
    if (oSavedColumnVisibility != null && oSavedColumnVisibility != undefined) return oSavedColumnVisibility;
    throw new Exception();

  } catch (e) {
    WeatherStation.LocalStorage.saveData("SearchableTableColumnVisibility", {});
    return {};
  }
}

function initalizeSavedColumnVisibility(iSiteId) {

  let oLoadedSiteSettings = getLoadedSiteViewSettings();

  let oNewSavedColumnVisibility = {};
  // Initalize visibility
  var aKeys = Object.keys(oLoadedSiteSettings);
  for (var i = 0; i < aKeys.length; i++) {
    let oColumnViewSetting = oLoadedSiteSettings[aKeys[i]];
    if (oColumnViewSetting.enabled) {
      oNewSavedColumnVisibility[aKeys[i]] = true;
    }
  }
  updateAndSaveColumnVisibility(iSiteId, oNewSavedColumnVisibility);

  return oNewSavedColumnVisibility;
}

function getSavedColumnVisibility(iSiteId) {

  let oAllSavedColumnVisibility = getAllSavedColumnVisibility();

  if (oAllSavedColumnVisibility[iSiteId] != undefined) return oAllSavedColumnVisibility[iSiteId];
  return initalizeSavedColumnVisibility(iSiteId);
}

function updateAndSaveColumnVisibility(iSiteId, oNewSavedColumnVisibility) {

  let oAllSavedColumnVisibility = getAllSavedColumnVisibility();
  oAllSavedColumnVisibility[iSiteId] = oNewSavedColumnVisibility;

  WeatherStation.LocalStorage.saveData("SearchableTableColumnVisibility", oAllSavedColumnVisibility);
}

function getEnabledTableColumns(iSiteId) {

  let oSiteViewSettings = getLoadedSiteViewSettings();
  let oSavedColumnVisibility = getSavedColumnVisibility(iSiteId);

  let aKeys = Object.keys(oSiteViewSettings);
  for (var i = 0; i < aKeys.length; i++) {

    if ((oSavedColumnVisibility[aKeys[i]] == true) && oSiteViewSettings[aKeys[i]].enabled) {
      oSiteViewSettings[aKeys[i]].show = true;
    } else {
      oSiteViewSettings[aKeys[i]].show = false;
    }
  }

  return oSiteViewSettings;
}

export function openColumnVisibilityToggle(iStationId, iSiteId) {

  let oSiteViewSettings = getLoadedSiteViewSettings();
  let oSavedColumnVisibility = getSavedColumnVisibility(iSiteId);

  let aKeys = Object.keys(oSiteViewSettings);
  for (var i = 0; i < aKeys.length; i++) {

    if (oSavedColumnVisibility[aKeys[i]] == true) {
      oSiteViewSettings[aKeys[i]].checked = true;
    } else {
      oSiteViewSettings[aKeys[i]].checked = false;
    }
  }

  let oData = {};
  oData.stationId = iStationId;
  oData.siteId = iSiteId;
  oData.tableColumnSettings = oSiteViewSettings;

  WeatherStation.Util.executeHandlebars('modal-content', 'TableColumnToggle', oData, 'clear');
}

export function applyColumnVisibilityToggle(iStationId, iSiteId) {

  let eCheckboxes = document.getElementsByClassName('form-check-input');
  let oSavedColumnVisibility = {};
  for (let i = 0; i < eCheckboxes.length; i++) {
    var eCheckbox = eCheckboxes.item(i);
    oSavedColumnVisibility[eCheckbox.value] = eCheckbox.checked;
  }

  updateAndSaveColumnVisibility(iSiteId, oSavedColumnVisibility);
  onSearch(iStationId, iSiteId);
  WeatherStation.Util.closeModal();
}

/**
 *
 * DOWNLOADS
 *
 */
export function openDataDownloadModal(iStationId, iSiteId) {

  let oData = {};
  oData.stationId = iStationId;
  oData.siteId = iSiteId;

  WeatherStation.Util.executeHandlebars('modal-content', 'TableDataDownloadTypes', oData, 'clear');
}

export function onDownload(iStationId, iSiteId) {

  document.getElementById('download-error-holder').innerHTML = '';

  let sFormat;

  let eCheckboxes = document.getElementsByClassName('form-check-input');
  for (let i = 0; i < eCheckboxes.length; i++) {
    var eCheckbox = eCheckboxes.item(i);
    if (eCheckbox.checked) {
      sFormat = eCheckbox.value;
    }
  }

  if (!sFormat) {
    document.getElementById('download-error-holder').innerHTML = 'No format selected';
    return;
  }

  let oDates = WeatherStation.Util.validateDateFields();
  if (!oDates) {
    WeatherStation.Util.closeModal();
    return false;
  }

  getFileData(iStationId, iSiteId, oDates, sFormat);

}

function getFileData(iStationId, iSiteId, oDates, sFileFormat) {

  let sSort = '';
  let sSortDir = '';

  let oSortDetails = getSortColumn(iSiteId);
  if (oSortDetails.sort != undefined && oSortDetails.sort != "") {
    sSort = oSortDetails.sort;
  } else {
    sSort = 'timestamp';
  }
  if (oSortDetails.sortDir != undefined && oSortDetails.sortDir != "") {
    sSortDir = oSortDetails.sortDir;
  } else {
    sSortDir = 'DESC';
  }

  let oPageableRequest = new WeatherStation.Class.PageableRequest();
  oPageableRequest.startDate = oDates.startDate;
  oPageableRequest.endDate = oDates.endDate;
  oPageableRequest.ignorePageSize = true;
  oPageableRequest.sort = sSort;
  oPageableRequest.sortDir = oPageableRequest.sortDirs[sSortDir];

  let oSavedDataSourceSettings = getSavedDataSourceSettings(iSiteId);

  if (oSavedDataSourceSettings.recent && oSavedDataSourceSettings.truncated) {
    oPageableRequest.source = oPageableRequest.dataSources.BOTH;
  } else if (oSavedDataSourceSettings.recent) {
    oPageableRequest.source = oPageableRequest.dataSources.RECENT;
  } else if (oSavedDataSourceSettings.truncated) {
    oPageableRequest.source = oPageableRequest.dataSources.TRUNCATED;
  } else {
    return;
  }

  document.getElementById('downloadButton').disabled = true;
  document.getElementById('downloadLoadingGif').classList.remove('invisible');

  WeatherStation.DataService.getStationRecords(iStationId, iSiteId, oPageableRequest).then(function(oPageableResponse) {

    if (!WeatherStation.Util.validateWebserviceCall(oPageableResponse)) {
      return;
    }

    document.getElementById('downloadButton').disabled = false;
    document.getElementById('downloadLoadingGif').classList.add('invisible');

    if (oPageableResponse.content.length <= 0) {
      document.getElementById('download-error-holder').innerHTML = 'No data records found';
    }

    let oLoadedSiteViewSettings = getLoadedSiteViewSettings();
    let oOrderedSiteViewSettings = orderSiteSettings(oLoadedSiteViewSettings);

    let aFieldNames = [];
    oOrderedSiteViewSettings.forEach((setting) => {
      aFieldNames.push(setting.key);
    });

    switch (sFileFormat) {

      case 'csv':
        generateCsvFile(oPageableResponse, aFieldNames, oDates);
    }

  });
}

function generateCsvFile(oPageableResponse, aFieldNames, oDates) {

  let sFileName = `weatherRecords_${oDates.startDate}_${oDates.endDate}.csv`;
  let sFileType = 'data:text/csv;charset=utf-8,';

  let csv = '';

  csv += aFieldNames.join(',');
  csv += '\n';

  let aContent = oPageableResponse.content;

  for (let i = 0; i < aContent.length; i++) {

    let aValues = [];
    aFieldNames.forEach((field) => {
      aValues.push(aContent[i][field]);
    });

    csv += aValues.join(',');
    csv += '\n';
  }

  downloadFile(csv, sFileType, sFileName);
}

function downloadFile(sFile, sFileType, sFileName) {

  let eHiddenElement = document.createElement('a');
  eHiddenElement.href = sFileType + encodeURI(sFile);
  eHiddenElement.target = '_blank';
  eHiddenElement.download = sFileName;
  eHiddenElement.click();

  WeatherStation.Util.closeModal();
}

/**
 *
 * DATA SOURCE
 *
 */
export function openDataSourceModal(iStation, iSiteId) {

  let oData = {};

  let oSettings = getSavedDataSourceSettings(iSiteId);

  oData.stationId = iStation;
  oData.siteId = iSiteId;
  oData.settings = oSettings;

  WeatherStation.Util.executeHandlebars('modal-content', 'TableDataSettings', oData, 'clear');
}

export function applyDataSource(iStationId, iSiteId) {

  let oAllSavedDataSourceSettings = {};
  let eCheckboxes = document.getElementsByClassName('form-check-input');
  for (let i = 0; i < eCheckboxes.length; i++) {
    var eCheckbox = eCheckboxes.item(i);
    oAllSavedDataSourceSettings[eCheckbox.value] = eCheckbox.checked;
  }

  updateAndSaveDataSourceSettings(iSiteId, oAllSavedDataSourceSettings);
  onSearch(iStationId, iSiteId)
  WeatherStation.Util.closeModal();
}

function getAllSavedDataSourceSettings() {

  let sSavedDataSettings = WeatherStation.LocalStorage.getData("SearchableTableDataSources");

  try {
    let oSavedDataSettings = JSON.parse(sSavedDataSettings);
    if (oSavedDataSettings != null && oSavedDataSettings != undefined) return oSavedDataSettings;
    throw new Exception();

  } catch (e) {
    WeatherStation.LocalStorage.saveData("SearchableTableDataSources", {});
    return {};
  }
}

function getSavedDataSourceSettings(iSiteId) {

  let oAllSavedDataSourceSettings = getAllSavedDataSourceSettings();
  if (oAllSavedDataSourceSettings[iSiteId] != undefined) return oAllSavedDataSourceSettings[iSiteId];
  return initalizeSavedDataSourceSettings(iSiteId);
}

function initalizeSavedDataSourceSettings(iSiteId) {

  let oNewSavedDataSourceSettings = {};
  oNewSavedDataSourceSettings.recent = true;
  oNewSavedDataSourceSettings.truncated = true;
  updateAndSaveDataSourceSettings(iSiteId, oNewSavedDataSourceSettings);
  return oNewSavedDataSourceSettings;
}

function updateAndSaveDataSourceSettings(iSiteId, oNewSavedDataSourceSettings) {

  let oAllSavedDataSourceSettings = getAllSavedDataSourceSettings();
  oAllSavedDataSourceSettings[iSiteId] = oNewSavedDataSourceSettings;
  WeatherStation.LocalStorage.saveData("SearchableTableDataSources", oAllSavedDataSourceSettings);
}
