export function saveData(sKey, oData) {

  localStorage.setItem(sKey, JSON.stringify(oData));
}

export function getData(sKey) {

  return localStorage.getItem(sKey);

}

export function deleteData(sKey) {

  localStorage.removeItem(sKey);
}
