let oChartColors = {
  red: 'rgb(255, 99, 132)',
  orange: 'rgb(255, 159, 64)',
  yellow: 'rgb(255, 205, 86)',
  green: 'rgb(75, 192, 192)',
  blue: 'rgb(54, 162, 235)',
  purple: 'rgb(153, 102, 255)',
  grey: 'rgb(201, 203, 207)'
}

export function renderTwelveHourChart(oSite, aRecords, aCharts) {

  let oData = {};
  oData.records = aRecords;
  oData.site = oSite;

  WeatherStation.Util.executeHandlebars(`twelvehr-chart-holder-${oSite.id}`, 'TwelveHourChart', oData, 'clear');

  if (oData.records.length <= 0) {
    return;
  }

  var oRenderedViewSettings = oSite.renderedViewSettings.records;

  let aTimestamps = [];
  let aTemperatures = [];
  let aDewpoints = [];
  let aHumidity = [];
  let aPressure = [];
  let windSpeed = [];
  var i = 0;

  for (i = 0; i < aRecords.length; i++) {

    let oDateTime = WeatherStation.Util.parseInstantDateTime(aRecords[i].timestamp);

    aTimestamps.push(WeatherStation.Util.padHour(oDateTime.hour) + ':' + WeatherStation.Util.appendLeadingZeroes(oDateTime.minute));
    aTemperatures.push(aRecords[i].temp1);
    aDewpoints.push(aRecords[i].dewPoint);
    aHumidity.push(aRecords[i].humidity);
    aPressure.push(aRecords[i].pressure);
    windSpeed.push(aRecords[i].windSpeed);

  }
  aTimestamps = aTimestamps.reverse();

  let oDataSets = [];

  if (oRenderedViewSettings.temp1.enabled) {

    aTemperatures = aTemperatures.reverse();
    oDataSets.push({
      label: 'Temperature [°F]',
      backgroundColor: oChartColors.red,
      borderColor: oChartColors.red,
      data: aTemperatures,
      fill: false,
      yAxisID: 'y-axis-1'
    });
  }
  if (oRenderedViewSettings.dewPoint.enabled) {
    aDewpoints = aDewpoints.reverse();
    oDataSets.push({
      label: 'Dew Point [°F]',
      backgroundColor: oChartColors.blue,
      borderColor: oChartColors.blue,
      data: aDewpoints,
      fill: false,
      yAxisID: 'y-axis-1'
    });
  }
  if (oRenderedViewSettings.humidity.enabled) {
    aHumidity = aHumidity.reverse();
    oDataSets.push({
      label: 'Humidity [%]',
      backgroundColor: oChartColors.yellow,
      borderColor: oChartColors.yellow,
      data: aHumidity,
      fill: false,
      yAxisID: 'y-axis-1'
    });
  }
  if (oRenderedViewSettings.windSpeed.enabled) {
    windSpeed = windSpeed.reverse();
    oDataSets.push({
      label: 'Wind Speed [mph]',
      backgroundColor: oChartColors.green,
      borderColor: oChartColors.green,
      data: windSpeed,
      fill: false,
      yAxisID: 'y-axis-1'
    });
  }
  if (oRenderedViewSettings.pressure.enabled) {
    aPressure = aPressure.reverse();
    oDataSets.push({
      label: 'Pressure [mbar]',
      backgroundColor: oChartColors.grey,
      borderColor: oChartColors.grey,
      data: aPressure,
      fill: false,
      yAxisID: 'y-axis-2'
    });
  }

  var graphConfig = {
    type: 'line',
    data: {
      labels: aTimestamps,
      datasets: oDataSets
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      stacked: false,
      title: {
        display: true,
        text: 'Past 12 Hours'
      },
      scales: {
        yAxes: [{
          type: 'linear',
          display: true,
          position: 'left',
          id: 'y-axis-1',
        }, {
          type: 'linear',
          display: true,
          position: 'right',
          id: 'y-axis-2',

          // grid line settings
          gridLines: {
            drawOnChartArea: false, // only want the grid lines for one axis to show up
          },
        }],
      }
    }
  }

  var ctx = document.getElementById(`myChart-${oSite.id}`).getContext('2d');
  window['myChart-'+oSite.id] = new Chart(ctx, graphConfig);

  function parseDateTimeHHmm(dateTime) {

    let date = new Date(dateTime);
    return WeatherStation.Util.padHour(date.getHours()) + ":" + WeatherStation.Util.appendLeadingZeroes(date.getMinutes());
  }

}
