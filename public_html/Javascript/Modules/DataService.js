export function getAllActiveStationSummaries() {

  let oRequest = new WeatherStation.Class.WebserviceRequest('GET', {}, 'json', '/weather/stations/summaries', true);
  return oRequest.makeRequest().then(function(oResponse, sStatusText, oXhrObj) {

    return unpackResponse(oResponse);

  }, function(oXhrObj, sTextStatus, oErr) {

    return WeatherStation.Util.buildErrorMessage('Error loading stations.', oXhrObj);
  });

  function unpackResponse(oResponse) {

    if (oResponse) {
      return oResponse;
    } else {
      return [];
    }
  }
}

export function getAllActiveStations() {

  let oRequest = new WeatherStation.Class.WebserviceRequest('GET', {}, 'json', '/weather/stations', true);
  return oRequest.makeRequest().then(function(oResponse, sStatusText, oXhrObj) {

    return unpackResponse(oResponse);

  }, function(oXhrObj, sTextStatus, oErr) {

    return WeatherStation.Util.buildErrorMessage('Error loading stations.', oXhrObj);
  });

  function unpackResponse(oResponse) {

    if (oResponse) {
      return oResponse;
    } else {
      return [];
    }
  }
}

export function getStationDetails(iStationId) {

  let oRequest = new WeatherStation.Class.WebserviceRequest('GET', {}, 'json', `/weather/stations/${iStationId}/details`, true);
  return oRequest.makeRequest().then(function(oResponse, sStatusText, oXhrObj) {

    return oResponse;

  }, function(oXhrObj, sTextStatus, oErr) {

    return WeatherStation.Util.buildErrorMessage('Error loading station details.', oXhrObj);
  });

}

export function getLatestSiteRecord(iStationId, iSiteId) {

  let oRequest = new WeatherStation.Class.WebserviceRequest('GET', {}, 'json', `/weather/stations/${iStationId}/site/${iSiteId}/data/latest`, true);
  return oRequest.makeRequest().then(function(oResponse, sStatusText, oXhrObj) {

    return oResponse;

  }, function(oXhrObj, sTextStatus, oErr) {

    return WeatherStation.Util.buildErrorMessage('Error loading latest site record.', oXhrObj);
  });
}


// Modify to use new pagenation API
export function getStationRecords(iStationId, iSiteId, oPageableRequest) {

  let oRequest = new WeatherStation.Class.WebserviceRequest('GET', {}, 'json', `/weather/stations/${iStationId}/site/${iSiteId}/data?${oPageableRequest.toURL()}`, true);
  return oRequest.makeRequest().then(function(oResponse, sStatusText, oXhrObj) {

    return oResponse;

  }, function(oXhrObj, sTextStatus, oErr) {

    return WeatherStation.Util.buildErrorMessage('Error loading station records.', oXhrObj);
  });

}

export function getStationStatus(iStationId) {

  let oRequest = new WeatherStation.Class.WebserviceRequest('GET', {}, 'json', `/weather/stations/status/${iStationId}`, true);
  return oRequest.makeRequest().then(function(oResponse, sStatusText, oXhrObj) {

    return oResponse;

  }, function(oXhrObj, sTextStatus, oErr) {

    return WeatherStation.Util.buildErrorMessage('Error loading station status.', oXhrObj);
  });

}
