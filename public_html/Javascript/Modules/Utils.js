export function callWebservice(type, dataArray, dataType, call, auth, processData, callback) {

  var headersArray = {};

  $.ajax({
    type: type,
    processData: processData,
    headers: headersArray,
    data: dataArray,
    dataType: dataType,
    contentType: 'application/json',
    url: Weather.Config.ROOT_WEBSERVICE_URL + call,

    success: function(data, status, xhr) {
      response = {
        "status": "success",
        "body": data,
        "xhr": xhr
      };
    },

    error: function(data) {

      response = {
        "status": "error",
        "body": data
      };
    },

    complete: function(data) {

      // If auth error, display login dialog
      if (response.body && response.body.status && response.body.status === 403) {

        Weather.Auth.authenticateWebservice(auth.callback);

        return;
      }

      if (callback && typeof(callback) === "function") {
        callback(response);
      }
      return response;
    }
  });
}

export function validateWebserviceCall(oResult, sErrorHolder) {

  if (oResult instanceof WeatherStation.Class.Message) {
    oResult.setType("ERR");
    oResult.setAppend(false);

    if (sErrorHolder != undefined && sErrorHolder != "") {

      oResult.displayText(sErrorHolder);
    } else {
      oResult.displayModal();
    }

    return false;
  }
  return true;
}

export function validateListLengthGreaterThanZero(aArray, oErrorMessage, sErrorHolder) {

  if(!aArray instanceof Array) {
    showError(oErrorMessage, sErrorHolder);
    return false;
  }
  if(aArray.length == 0) {
    showError(oErrorMessage, sErrorHolder);
    return false;
  }

  return true;

  function showError(oErrorMessage, sErrorHolder) {

    if(oErrorMessage != undefined) {
      oErrorMessage.setType("ERR");
      oErrorMessage.setAppend(false);

      if (sErrorHolder != undefined && sErrorHolder != "") {
        oErrorMessage.displayText(sErrorHolder);

      } else {
        oErrorMessage.displayModal();
      }
    }
  }
}

export function buildErrorMessage(sErrorMessage, oXhrObj) {

  let message = new WeatherStation.Class.Message(sErrorMessage, oXhrObj.responseText);
  message.setType("ERR");

  return message;
}

/**
 * To compile templates (unix terminal) from public_html/core/: sudo handlebars -m [templates folder] -f [path/name of compiled file]
 * sudo handlebars -m js/templates/ -f js/templates/templates.js
 */
export function executeHandlebars(sTemplateHolderSelector, sTemplateName, oData, bAppendClearFlag, fCallback) {

  if (bAppendClearFlag === "clear") {
    $("#" + sTemplateHolderSelector).html("");
  }

  $("#" + sTemplateHolderSelector).append(Handlebars.templates[sTemplateName](oData));

  if (fCallback && typeof(fCallback) === "function") {
    fCallback.apply(this);
  }

  return true;
}

export function openModal(sSize, fCallback, aParameterList) {

  let eModal = document.getElementById('modal');
  let modal = new bootstrap.Modal(eModal, {});

  let eDialog = eModal.firstElementChild;

  switch(sSize) {

    case 'sm':
      eDialog.className = 'modal-dialog modal-sm';
      break;
    case 'lg':
      eDialog.className = 'modal-dialog modal-lg';
      break;
    case 'xl':
      eDialog.className = 'modal-dialog modal-xl';
      break;
    default:
      eDialog.className = 'modal-dialog';
  }

  modal.show();

  if (fCallback && typeof(fCallback) === "function") {
    fCallback.apply(this, aParameterList);
  }
}

export function closeModal() {

  var eModal = document.getElementById('modal')
  var modal = bootstrap.Modal.getInstance(eModal)

  modal.hide();

  //let oBackDropElems = document.getElementsByClassName('modal-backdrop');
  //if(isDefault) {
  let oBackDropElems = document.getElementsByClassName('modal-backdrop');
  if(oBackDropElems.length > 0) {
    if(oBackDropElems[0].style) {
      oBackDropElems[0].style.display = "none";
    }
  }
  //}

}

export function appendLeadingZeroes(iNum){
  if(iNum <= 9){
    return "0" + iNum;
  }
  return iNum;
}

export function padHour(iHour) {
  if(iHour <= 9) {
    return "0"+iHour;
  }
  return iHour;
}

export function addElementClass(sElementId, sClassToAdd) {
  document.getElementById(sElementId).classList.add(sClassToAdd);
}

export function removeElementClass(sElementId, sClassToRemove) {
  document.getElementById(sElementId).classList.remove(sClassToRemove);
}

// 2021-02-26T10:00:23Z
export function parseInstantDateTime(sDateTime) {

  let oDateTime = {};

  sDateTime = sDateTime.replace('Z', '');

  let aSplitDateTime = sDateTime.split('T');

  let aDate = aSplitDateTime[0].split('-');
  let aTime = aSplitDateTime[1].split(':');

  const aShortMonths = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  oDateTime.year = parseInt(aDate[0], 10);
  oDateTime.month = parseInt(aDate[1], 10);
  oDateTime.sMonth = aShortMonths[parseInt(oDateTime.month, 10)-1];
  oDateTime.day = parseInt(aDate[2], 10);

  oDateTime.hour = parseInt(aTime[0], 10);
  oDateTime.minute = parseInt(aTime[1], 10);
  oDateTime.second = parseInt(aTime[2], 10);

  return oDateTime;
}

export function formatDate(oDateTime, sFormat) {

  var sYear = Intl.DateTimeFormat('en', {
    year: 'numeric'
  }).format(oDateTime);
  var sMonth = Intl.DateTimeFormat('en', {
    month: '2-digit'
  }).format(oDateTime);
  var sDay = Intl.DateTimeFormat('en', {
    day: '2-digit'
  }).format(oDateTime);
  var sHour = Intl.DateTimeFormat('en', {
    hour: '2-digit',
    hour12: false
  }).format(oDateTime);
  var sMinute = Intl.DateTimeFormat('en', {
    minute: '2-digit'
  }).format(oDateTime);

  if (sMinute.length == 1) {
    sMinute = "0" + sMinute;
  }

  if(sHour >= 24) {
    sHour = '00';
  }

  if(sFormat === "YYMMDDHHmm") {
    return sYear + sMonth + sDay + sHour + sMinute;
  }
  if(sFormat === "HHmm") {
    return sHour + sMinute;
  }

}

export function validateField(sFieldName, sFieldErrorHolder, sValue, aValidations) {

  removeFieldError(sFieldName, sFieldErrorHolder);

  let bIsValid = true;

  for (var i = 0; i < aValidations.length; i++) {
    var oValidation = aValidations[i];

    if (!oValidation.validation) return;

    if (oValidation.validation === "required") {
      if (sValue == "" || sValue == null) {
        addFieldError(sFieldName, sFieldErrorHolder, "Value is required.");
        bIsValid = false;
        break;
      }
    }
    if (oValidation.validation === "number") {

      let fValue = parseFloat(sValue);

      if (!Number.isFinite(fValue)) {
        addFieldError(sFieldName, sFieldErrorHolder, "Value must be a number.");
        bIsValid = false;
        break;
      }
      if (Number.isFinite(oValidation.gt)) {
        if (!(fValue > oValidation.gt)) {
          addFieldError(sFieldName, sFieldErrorHolder, "Value must be greater than " + oValidation.gt);
          bIsValid = false;
          break;
        }
      }
      if (Number.isFinite(oValidation.gte)) {
        if (!(fValue >= oValidation.gte)) {
          addFieldError(sFieldName, sFieldErrorHolder, "Value must be greater than or equal to " + oValidation.gte);
          bIsValid = false;
          break;
        }
      }
      if (Number.isFinite(oValidation.lt)) {
        if (!(fValue < oValidation.lt)) {
          addFieldError(sFieldName, sFieldErrorHolder, "Value must be less than " + oValidation.lt);
          bIsValid = false;
          break;
        }
      }
      if (Number.isFinite(oValidation.lte)) {
        if (!(fValue <= oValidation.lte)) {
          addFieldError(sFieldName, sFieldErrorHolder, "Value must be less than or equal to " + oValidation.lte);
          bIsValid = false;
          break;
        }
      }
    }
  }

  return bIsValid;
}

export function addFieldError(sFieldId, sFieldErrorHolderId, sErrorMessage) {
  addInputFieldErrorStyling(sFieldId);
  setFieldError(sFieldErrorHolderId, sErrorMessage);
  let oField = document.getElementById(sFieldId);
  oField.focus();
}

export function removeFieldError(sFieldId, sFieldErrorHolderId) {
  clearInputFieldStyling(sFieldId);
  setFieldError(sFieldErrorHolderId, "");
}

export function setFieldError(sErrorHolderId, sMessage) {
  $("#" + sErrorHolderId).html(sMessage);
}

export function addInputFieldErrorStyling(sElementId) {
  setElementClass(sElementId, "form-control is-invalid");
}

export function clearInputFieldStyling(sElementId) {
  setElementClass(sElementId, "form-control");
}

export function setElementClass(sElementId, sClassToSet) {

  document.getElementById(sElementId).className = sClassToSet;
}

export function executeFunctionByName(functionName, context, args) {

  var args = Array.prototype.slice.call(arguments, 2);
  var namespaces = functionName.split(".");
  var func = namespaces.pop();
  for (var i = 0; i < namespaces.length; i++) {
    context = context[namespaces[i]];
  }
  return context[func].apply(context, args);
}

export function validateDateFields() {

  let bIsValid = true;

  let aDateFieldValidation = [{
    validation: 'required'
  }];

  let sStartDate = document.getElementById('startDateTime').value;
  if (!WeatherStation.Util.validateField('startDateTime', 'startDateTimeErrorHolder', sStartDate, aDateFieldValidation)) bIsValid = false;

  let sEndDate = document.getElementById('endDateTime').value;
  if (!WeatherStation.Util.validateField('endDateTime', 'endDateTimeErrorHolder', sEndDate, aDateFieldValidation)) bIsValid = false;

  if (!bIsValid) return false;

  if (!validateStartDateBeforeEnd(sStartDate, sEndDate)) return false;

  try {
    sStartDate = WeatherStation.Util.formatDate(new Date(sStartDate), "YYMMDDHHmm");
    sEndDate = WeatherStation.Util.formatDate(new Date(sEndDate), "YYMMDDHHmm");

  } catch (e) {
    return false;
  }

  return {
    startDate: sStartDate,
    endDate: sEndDate
  };
}

function validateStartDateBeforeEnd(sStartDate, sEndDate) {

  WeatherStation.Util.removeFieldError('startDateTime', 'startDateTimeErrorHolder');
  WeatherStation.Util.removeFieldError('endDateTime', 'endDateTimeErrorHolder');

  if (parseInt(stripDate(sStartDate), 10) >= parseInt(stripDate(sEndDate), 10)) {
    WeatherStation.Util.addFieldError('startDateTime', 'startDateTimeErrorHolder', 'Start date must be before end date.');
    WeatherStation.Util.addFieldError('endDateTime', 'endDateTimeErrorHolder', 'Start date must be before end date.');
    return false;
  }
  return true;
}

function stripDate(sDate) {
  return sDate.replace(/-|T|:/g, '');
}
