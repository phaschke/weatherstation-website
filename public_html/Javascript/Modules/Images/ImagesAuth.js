
export function authenticateUser(oCallbackDetails) {

  HomeAutomation.Util.openModal(this.openLoginModal(oCallbackDetails));

}

export function openLoginModal(sCallback) {

  let oData = {};
  oData.callback = sCallback;

  WeatherStation.Util.executeHandlebars('modal-content', 'LoginModal', oData, "clear");
}

export function attemptAuthentication(sCallbackDetails) {

  let bIsValid = true;

  let aValidation = [{
    validation: 'required'
  }];

  let sUsername = document.getElementById('usernameField').value;
  if (!WeatherStation.Util.validateField('usernameField', 'usernameErrorHolder', sUsername, aValidation)) bIsValid = false;

  let sPassword = document.getElementById('passwordField').value;
  if (!WeatherStation.Util.validateField('passwordField', 'passwordErrorHolder', sPassword, aValidation)) bIsValid = false;

  if(!bIsValid) return;

  var oBody = {
    "username": sUsername,
    "password": sPassword
  }

  var oAuthData = {
    "check": false
  };

  WeatherStation.Util.setFieldError("loginResultErrorHolder", "");

  return Promise.resolve(

    $.ajax({
      type: "POST",
      processData: false,
      headers: {},
      data: JSON.stringify(oBody),
      dataType: "text",
      contentType: 'application/json',
      url: WeatherStation.Config.IMAGE_WEBSERVICE_URL + "/api/authenticate",

      complete: function(response) {

        if (response && response.status == 403) {

          WeatherStation.Util.setFieldError("loginResultErrorHolder", "Failed to Authenticate, please try again.");

          return;
        }

        if (response && response.status == 200) {

          var token = "Bearer " + response.getResponseHeader("Authorization");
          localStorage.setItem("token", token);

          WeatherStation.Util.closeModal();

          if(sCallbackDetails) {
            try {
              let oCallbackDetails = JSON.parse(sCallbackDetails);
              WeatherStation.Util.executeFunctionByName(`${oCallbackDetails.namespace}.${oCallbackDetails.func}`, window, oCallbackDetails.params);

            } catch(error) {
              console.log(error);
              return false;
            }
          }

        }
      }

    })
  );
}

export function clearAuthToken(sCallbackDetails) {

  localStorage.setItem('token', '');

  if(sCallbackDetails) {
    try {
      let oCallbackDetails = JSON.parse(sCallbackDetails);
      WeatherStation.Util.executeFunctionByName(`${oCallbackDetails.namespace}.${oCallbackDetails.func}`, window, oCallbackDetails.params);

    } catch(error) {
      console.log(error);
      return false;
    }
  }
}

export function getUserDetails() {

  let oResult = {};

  var token = localStorage.getItem('token');

  if (token && token !== '') {

    let jwt = token.replace('Bearer Bearer ','');
    let jwtData = jwt.split('.')[1];
    let decodedJwtJsonData = window.atob(jwtData);
    let decodedJwtData = JSON.parse(decodedJwtJsonData);

    oResult.username = decodedJwtData.sub;
    oResult.loggedIn = true;
    oResult.role = 'GUEST';

    if(decodedJwtData.rol.indexOf('ROLE_ADMIN') > -1) {
      oResult.role = 'ADMIN';
    }
    if(decodedJwtData.rol.indexOf('ROLE_USER') > -1) {
      oResult.role = 'USER';
    }

  } else {
    oResult.loggedIn = false;
  }


  return oResult;

}
