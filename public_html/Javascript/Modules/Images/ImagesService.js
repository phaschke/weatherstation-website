
export function getImageLocationFromWeatherStationId(iStationId) {

  let oRequest = {
    type: 'GET',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/location/weather-station/${iStationId}`,
    authenticate: false
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Image Location', xhrObj);
  });

}

export function getWeatherStationIdFromLocationId(iLocationId) {

  let oRequest = {
    type: 'GET',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/weather-station/location/${iLocationId}`,
    authenticate: false
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Weather Station Id From Location', xhrObj);
  });

}

export function getImageLocation(iLocationId) {

  let oRequest = {
    type: 'GET',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/location/${iLocationId}`,
    authenticate: false
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Image Location', xhrObj);
  });

}

export function getCamsWithRecentThumbnails(iLocationId) {

  let oRequest = {
    type: 'GET',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/thumbnails/location/${iLocationId}`,
    authenticate: false
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Cams and Thumbnails', xhrObj);
  });
}

export function getImageDetails(iImageId) {

  let oRequest = {
    type: 'GET',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/image/${iImageId}/details`,
    authenticate: false
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Image Details', xhrObj);
  });
}

export function getCamDetails(iCamId) {

  let oRequest = {
    type: 'GET',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/cam/${iCamId}`,
    authenticate: false
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Cam Details', xhrObj);
  });
}

export function getImage(iImageId) {

  let oRequest = {
    type: 'GET',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/image/base64/${iImageId}`,
    authenticate: false
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Image', xhrObj);
  });
}

export function getImageSearch(iCamId, oPageableRequest) {

  let oRequest = {
    type: 'GET',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/search/${iCamId}?${oPageableRequest.toURL()}`,
    authenticate: false
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Image Search Results', xhrObj);
  });
}

export function retainImage(iImageId) {

  let oRequest = {
    type: 'PATCH',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/image/retain/${iImageId}`,
    authenticate: true
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    if(xhrObj && xhrObj.status && xhrObj.status == 200) {
      return true;
    }

    return buildErrorMessage('Error Retaining Image', xhrObj);
  });

}

export function unretainImage(iImageId) {

  let oRequest = {
    type: 'PATCH',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/image/unretain/${iImageId}`,
    authenticate: true
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    if(xhrObj && xhrObj.status && xhrObj.status == 200) {
      return true;
    }

    return buildErrorMessage('Error Unretaining Image', xhrObj);
  });
}

export function deleteImage(iImageId) {

  let oRequest = {
    type: 'DELETE',
    processData: true,
    data: {},
    dataType: 'json',
    uri: `/api/images/image/${iImageId}`,
    authenticate: true
  };

  return makeRequest(oRequest).then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Deleting Image', xhrObj);
  });
}


function makeRequest(oRequest) {

  oRequest.headers = {};

  //if(oRequest.authenticate) {
  let token = localStorage.getItem("token");
  if (token && token !== "") {
    oRequest.headers = {
        "Authorization": token
    };
  }
  //}

  return Promise.resolve(
    $.ajax({
      type: oRequest.type,
      processData: oRequest.processData,
      headers: oRequest.headers,
      data: oRequest.data,
      dataType: oRequest.dataType,
      contentType: 'application/json',
      url: WeatherStation.Config.IMAGE_WEBSERVICE_URL + oRequest.uri,

      complete: function(data) {

        /*if (fCallback && typeof(fCallback) === "function") {
          fCallback(data);
        }*/
        return data;
      }
    })
  );
}

function buildErrorMessage(sErrorMessage, oXhrObj) {

  let oMessage = new WeatherStation.Class.Message(sErrorMessage, oXhrObj.responseText);
  oMessage.setType("ERR");

  return oMessage;
}

export function validateWebserviceCall(oResult, sErrorHolder) {

  if (oResult instanceof WeatherStation.Class.Message) {
    oResult.setType("ERR");
    oResult.setAppend(false);
    if (sErrorHolder != undefined && sErrorHolder != "") {
      oResult.displayText(sErrorHolder);
    } else {
      oResult.displayModal();
    }

    return false;
  }
  return true;
}
