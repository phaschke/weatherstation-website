export function initalizeStation(iStationId, iLocationId) {

  if(iStationId && iStationId >= 0) {
    initalizeLocationByStationId(iStationId);
  }
  if(iLocationId && iLocationId >= 0) {
    initalizeLocationByLocationId(iLocationId);
  }
}

export function initalizeLocationByLocationId(iLocationId) {

  // Get the associated weather station id from the location id for back arrow
  WeatherStation.Images.Service.getWeatherStationIdFromLocationId(iLocationId).then(function(oWeatherStation) {
    if(!WeatherStation.Images.Service.validateWebserviceCall(oWeatherStation)) return;

    let sBaseUrl = WeatherStation.Config.ROOT_URL;
    let eBackButtonHolder = document.getElementById('back-button');
    let sBackButtonHTML = `<a class="btn btn-outline-dark" title="Back to station" href="${sBaseUrl}/station/show/${oWeatherStation.id}"><i class="fas fa-arrow-left"></i> To Station</a>`;
    eBackButtonHolder.innerHTML = sBackButtonHTML;
  });

  if(iLocationId instanceof Array && iLocationId.length > 0) {
    iLocationId = iLocationId[0];
  }

  let oCallback = {
      namespace: 'WeatherStation.Images',
      func: 'initalizeLocationByLocationId',
      params: [iLocationId]
  };

  let sCallback = JSON.stringify(oCallback);
  sCallback = sCallback.replace('"', '\"');

  WeatherStation.Images.Service.getImageLocation(iLocationId).then(function(oImageLocation) {

    if(!WeatherStation.Images.Service.validateWebserviceCall(oImageLocation)) return;

    let oUserAuth = WeatherStation.Images.Auth.getUserDetails();

    let oData = {};
    oData.location = oImageLocation;
    oData.callback = sCallback;
    oData.userAuth = oUserAuth;

    WeatherStation.Util.executeHandlebars('location-holder', 'Location', oData, 'clear');

    getCams(oImageLocation);

  });
}


export function initalizeLocationByStationId(iStationId) {

  if(iStationId instanceof Array && iStationId.length > 0) {
    iStationId = iStationId[0];
  }

  let oCallback = {
      namespace: 'WeatherStation.Images',
      func: 'initalizeLocationByStationId',
      params: [iStationId]
  };

  let sCallback = JSON.stringify(oCallback);
  sCallback = sCallback.replace('"', '\"');

  WeatherStation.Images.Service.getImageLocationFromWeatherStationId(iStationId).then(function(oImageLocation) {

    if(!WeatherStation.Images.Service.validateWebserviceCall(oImageLocation)) return;

    let oUserAuth = WeatherStation.Images.Auth.getUserDetails();

    let oData = {};
    oData.location = oImageLocation;
    oData.callback = sCallback;
    oData.userAuth = oUserAuth;

    WeatherStation.Util.executeHandlebars('location-holder', 'Location', oData, 'clear');

    getCams(oImageLocation);

  });

}

export function getCams(oImageLocation) {

  WeatherStation.Images.Service.getCamsWithRecentThumbnails(oImageLocation.id).then(function(aCamsWithThumbnails) {

    if(!WeatherStation.Images.Service.validateWebserviceCall(aCamsWithThumbnails)) return;

    let oData = {};

    oData.baseUrl = WeatherStation.Config.ROOT_URL;

    oData.cams = aCamsWithThumbnails;

    oData.cams.forEach((cam) => {

      if(cam.imageDTO) {
        let oOffset = getDateOffset(cam.imageDTO.timestamp, oImageLocation);
        cam.timeOffsetText = formatTimeOffset(oOffset);

      }
    });

    WeatherStation.Util.executeHandlebars('cams-holder', 'CamAlbum', oData, 'clear');

  });

  function formatTimeOffset(oOffset) {
    let sOffset = '';
    if(oOffset.days > 0) {
      sOffset = `${oOffset.days} Days `;
    }
    if(oOffset.hours > 0) {
      sOffset += `${oOffset.hours} Hours `;
    }
    if(oOffset.minutes > 0 || sOffset.length == 0) {
      sOffset += `${oOffset.minutes} Minutes `;
    }
    sOffset += 'Ago';

    return sOffset;
  }
}

export function displayImageModal(iImageId) {

  WeatherStation.Images.Service.getImageDetails(iImageId).then(function(oImageDetails) {

    if(!WeatherStation.Images.Service.validateWebserviceCall(oImageDetails)) return;

    let oUserAuth = WeatherStation.Images.Auth.getUserDetails();

    let oData = {};
    oData.image = oImageDetails.imageDTO;
    oData.cam = oImageDetails.camDTO;
    oData.userAuth = oUserAuth;

    if(oData.cam.allowedRole != "ROLE_GUEST") {

      // The user requires the image to be retrieved with credentials
      WeatherStation.Images.Service.getImage(iImageId).then(function(oImage) {

        if(!WeatherStation.Images.Service.validateWebserviceCall(oImage)) return;

        oData.url = `data:image/jpeg;base64,${oImage.image}`;
        oData.usePrivillegedUrl = true;
        oData.privillegedUrl = `${WeatherStation.Config.ROOT_URL}/images/image/${iImageId}`;
        finishDisplayingImageModal(oData);
      });

    } else {

      oData.url = `${WeatherStation.Config.IMAGE_WEBSERVICE_URL}/api/images/image/${iImageId}`;
      finishDisplayingImageModal(oData);
    }

    function finishDisplayingImageModal(oData) {

      WeatherStation.Util.executeHandlebars('modal-content', 'ImageModal', oData, 'clear');

      // Initalize Clipboard.js
      var btn = document.getElementById('clipboard-test');
      var clipboard = new ClipboardJS(btn, {container: document.getElementById('modal')});

      clipboard.on('success', function (e) {
        //console.log(e);
      });

      clipboard.on('error', function (e) {
        console.log(e);
      });
    }

  });
}

export function downloadImage(iImageId) {

  WeatherStation.Images.Service.getImage(iImageId).then(function(oImage) {

    if(!WeatherStation.Images.Service.validateWebserviceCall(oImage)) return;

    let sDataUri = `data:application/octet-stream;base64,${oImage.image}`;;
    let sFilename = oImage.imageDetails.imageName;
    let x = $("<a download='" + sFilename + "' href='" + sDataUri + "'></a>");
    x.appendTo('body');
    x[0].click();
    x.remove();
  });
}

export function displayImage(iImageId) {

  WeatherStation.Images.Service.getImage(iImageId).then(function(oImage) {

    if(!WeatherStation.Images.Service.validateWebserviceCall(oImage, 'image-holder')) return;

    let oData = {};
    oData.url = `data:image/jpeg;base64,${oImage.image}`;

    WeatherStation.Util.executeHandlebars('image-holder', 'Image', oData, 'clear');
  });
}

export function retainImage(iImageId) {

  WeatherStation.Images.Service.retainImage(iImageId).then(function(oImage) {

    if(!WeatherStation.Images.Service.validateWebserviceCall(oImage)) return;

    displayImageModal(iImageId);
  });
}

export function unretainImage(iImageId) {

  WeatherStation.Images.Service.unretainImage(iImageId).then(function(oImage) {

    if(!WeatherStation.Images.Service.validateWebserviceCall(oImage)) return;

    displayImageModal(iImageId);
  });
}

export function displayDeleteImageConfirmation(iImageId) {

  let oData = {
    header: 'Confirm Deletion',
    message: 'Are you sure you want to delete the image?',
    deleteCallback: `WeatherStation.Images.deleteImage(${iImageId})`,
    cancelCallback: `WeatherStation.Images.displayImageModal(${iImageId})`
  };

  WeatherStation.Util.executeHandlebars('modal-content', 'DeleteConfirmation', oData, 'clear');
}

export function deleteImage(iImageId) {

  WeatherStation.Images.Service.deleteImage(iImageId).then(function(oLocation) {

    if(!WeatherStation.Images.Service.validateWebserviceCall(oLocation)) return;

    WeatherStation.Util.closeModal();
    getCams(oLocation);
  });
}

export function initalizeImageSearch(iCamId) {

  WeatherStation.Images.Service.getCamDetails(iCamId).then(function(oCam) {

    if (!WeatherStation.Util.validateWebserviceCall(oCam, 'search-title-holder')) {
      return;
    }

    let oData = oCam;

    WeatherStation.Util.executeHandlebars('search-title-holder', 'ImageSearchTitle', oData, 'clear');

  });
}

export function onSearch(iCamId, iPage, sSort, sSortDir) {

  let oDates = WeatherStation.Util.validateDateFields();
  if (!oDates) return false;

  let eDataTypeSelect = document.getElementById('dataTypeSelect');
  let sDataType = eDataTypeSelect.options[eDataTypeSelect.selectedIndex].value;

  sSort = 'timestamp';
  sSortDir = 'DESC';

  if (iPage == undefined || iPage == null) {
    iPage = 0;
  }

  let oData = {};
  oData.loading = true;

  WeatherStation.Util.executeHandlebars('image-album-holder', 'ImageAlbum', oData, 'clear');

  let oPageableRequest = new WeatherStation.Class.PageableRequest();
  oPageableRequest.startDate = oDates.startDate;
  oPageableRequest.endDate = oDates.endDate;
  oPageableRequest.page = iPage;
  oPageableRequest.size = 20;
  oPageableRequest.sort = sSort;
  oPageableRequest.sortDir = oPageableRequest.sortDirs[sSortDir];
  oPageableRequest.source = sDataType;

  getRequestedDataAndRenderTable(iCamId, oPageableRequest);

}

function getRequestedDataAndRenderTable(iCamId, oPageableRequest) {

  WeatherStation.Images.Service.getImageSearch(iCamId, oPageableRequest).then(function(oPageableResponse) {

    if (!WeatherStation.Util.validateWebserviceCall(oPageableResponse, 'image-album-holder')) {
      return;
    }

    let oData = {};

    if (oPageableResponse && oPageableResponse.content && oPageableResponse.content.length > 0) {

      oData.images = oPageableResponse.content;
      oData.camId = iCamId;
    }

    if (oPageableResponse && oPageableResponse.totalPages != undefined && oPageableResponse.totalElements != undefined) {
      let oPageableResponseClass = new WeatherStation.Class.PageableResponse(oPageableResponse);
      oData.pagenation = oPageableResponseClass.toDisplayJSON();
      oData.sortField = oPageableResponseClass.sortField;
      oData.sortDirection = oPageableResponseClass.sortDirection;
    }

    WeatherStation.Util.executeHandlebars('image-album-holder', 'ImageAlbum', oData, 'clear');

  });
}

function getDateOffset(sDatetime, oLocation) {

  let date = new Date();
  let sTimezone = oLocation.timeZone;

  let sYear = Intl.DateTimeFormat('en', {
    year: 'numeric',
    timeZone: sTimezone,
  }).format(date);
  let sMonth = Intl.DateTimeFormat('en', {
    month: '2-digit',
    timeZone: sTimezone,
  }).format(date);
  let sDay = Intl.DateTimeFormat('en', {
    day: '2-digit',
    timeZone: sTimezone,
  }).format(date);
  let sHour = Intl.DateTimeFormat('en', {
    hour: '2-digit',
    hour12: false,
    timeZone: sTimezone,
  }).format(date);
  let sMinute = Intl.DateTimeFormat('en', {
    minute: '2-digit',
    hour12: false,
    timeZone: sTimezone,
  }).format(date);
  let sSecond = Intl.DateTimeFormat('en', {
    second: '2-digit',
    hour12: false,
    timeZone: sTimezone,
  }).format(date);

  sDay = zeroPadValue(sDay);
  sHour = zeroPadValue(sHour);
  sMinute = zeroPadValue(sMinute);
  sSecond = zeroPadValue(sSecond);

  if(sHour >= 24) {
    sHour = '00';
  }

  let sDateTimeNow = `${sYear}-${sMonth}-${sDay}T${sHour}:${sMinute}:${sSecond}`;
  sDatetime = sDatetime.replace('Z', '');

  let dNow = new Date(sDateTimeNow);
  let dThen = new Date(sDatetime);

  console.log(dNow);
  console.log(dThen);

  let iDelta = Math.abs((dNow - dThen) / 1000);

  let iDaysBetween = Math.floor(iDelta / 86400);
  iDelta -= iDaysBetween * 86400;

  let iHoursBetween = Math.floor(iDelta / 3600) % 24;
  iDelta -= iHoursBetween * 3600;

  let iMinutesBetween = Math.floor(iDelta / 60) % 60;
  iDelta -= iMinutesBetween * 60;

  let oResult = {
    days: iDaysBetween,
    hours: iHoursBetween,
    minutes: iMinutesBetween
  }

  return oResult;
}

function zeroPadValue(sValue) {
  if (sValue.length == 1) {
    sValue = "0" + sValue;
  }
  return sValue;
}
