
// Utils
import * as utils from './Modules/Utils.js';
WeatherStation.Util = utils;

import * as localStorage from './Modules/LocalStorage.js';
WeatherStation.LocalStorage = localStorage;

WeatherStation.Class = WeatherStation.Class || {};
import * as messageClass from './Classes/Message.js';
import * as webserviceClass from './Classes/Webservice.js';
import * as pageableRequestClass from './Classes/PageableRequest.js';
import * as pageableResponseClass from './Classes/PageableResponse.js';
WeatherStation.Class = Object.assign({}, messageClass, webserviceClass, pageableRequestClass, pageableResponseClass);

import * as dataService from './Modules/DataService.js';
WeatherStation.DataService = dataService;

import * as stations from './Modules/Stations.js';
WeatherStation.Stations = stations;

import * as charts from './Modules/Charts.js';
WeatherStation.Charts = charts;

import * as tables from './Modules/Tables.js';
WeatherStation.Tables = tables;

WeatherStation.Images = WeatherStation.Images || {};
import * as images from './Modules/Images/Images.js';
WeatherStation.Images = Object.assign({}, images);

import * as imagesService from './Modules/Images/ImagesService.js';
WeatherStation.Images.Service = imagesService;

import * as imagesAuth from './Modules/Images/ImagesAuth.js';
WeatherStation.Images.Auth = imagesAuth;

/**
 * Register Handlebars helpers and partials
 */
import * as handlebarsSetup from './HandlebarsSetup.js';
handlebarsSetup.registerHelpers();
handlebarsSetup.registerPartials();
