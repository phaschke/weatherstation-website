export class Message {

  constructor(sHeader, sDetails) {
    this.header = sHeader;
    this.details = sDetails;
  }

  setHeader(sHeader) {
    this.header = sHeader;
  }

  setDetails(sDetails) {
    this.details = sDetails;
  }

  getDetails() {
    return this.details;
  }

  setType(type) {
    this.type = type;
  }

  setAppend(flag) {
    this.appendFlag = flag;
  }

  displayText(HTMLHolder) {
    if (!this.appendFlag) {
      // Change to true/false
      this.appendFlag = "clear";
    }

    let message = {
      "type": this.type,
      "header": this.header,
      "details": this.details
    }

    WeatherStation.Util.executeHandlebars(HTMLHolder, "MessageText", message, this.appendFlag);

  }

  defaultDetailsIfUndefined(toCheck, toDefault) {
    if (toCheck && toCheck !== undefined) {
      this.details = toCheck;
    } else {
      this.details = toDefault;
    }
  }

  displayModal() {

    let message = {
      "type": this.type,
      "header": this.header,
      "details": this.details
    }

    WeatherStation.Util.openModal('default', WeatherStation.Util.executeHandlebars("modal-content", "MessageModal", message, "clear"));

  }

}
