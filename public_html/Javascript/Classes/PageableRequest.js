export class PageableRequest {

  dataSources = {
    RECENT: 'RECENT',
    TRUNCATED: 'TRUNCATED',
    BOTH: 'BOTH',
    ALL: 'ALL',
    ONLY_RETAINED: 'ONLY_RETAINED',
    ONLY_NON_RETAINED: 'ONLY_NON_RETAINED'
  }

  sortDirs = {
    DESC: 'DESC',
    ASC: 'ASC'
  }

  constructor(sStartDate, sEndDate, sSource, iPage, iSize, sSort, sSortDir) {
    this.startDate = sStartDate;
    this.endDate = sEndDate;
    this.source = sSource;
    this.page = iPage;
    this.size = iSize;
    this.sort = sSort;
    this.sortDir = sSortDir;
    this.ignorePageSize = null;
  }

  toURL() {

    let sURL = `startdate=${this.startDate}&enddate=${this.endDate}&source=${this.source}`;
    if(this.page != undefined) {
      sURL = sURL+`&page=${this.page}`;
    }
    if(this.size != undefined) {
      sURL = sURL+`&size=${this.size}`;
    }
    if(this.sort && this.sortDir) {
      sURL = sURL+`&sort=${this.sort},${this.sortDir}`;
    }
    if(this.ignorePageSize != null) {
      sURL = sURL+`&ignorePageSize=${this.ignorePageSize}`;
    }

    return sURL;
  }

}
