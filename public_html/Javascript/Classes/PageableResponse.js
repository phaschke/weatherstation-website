export class PageableResponse {

  pageableDisplayNumber = 6;

  constructor(oPageableResponse) {

    this.isFirst = oPageableResponse.first;
    this.isLast = oPageableResponse.last;
    this.page = oPageableResponse.page;
    this.totalPages = oPageableResponse.totalPages;
    this.totalElements = oPageableResponse.totalElements;
    this.pageSize = oPageableResponse.pageSize;
    this.prevPage = null;
    this.nextPage = null;
    this.lastPage = (oPageableResponse.totalPages - 1);
    this.startRange = null;
    this.endRange = null;
    this.pages = [];
    this.sortField = oPageableResponse.sortField;
    this.sortDirection = oPageableResponse.sortDirection;
  }

  toDisplayJSON() {

    if (!this.isFirst) {
      this.prevPage = parseInt((this.page - 1), 10);
    }
    if (!this.isLast) {
      this.nextPage = parseInt((this.page + 1), 10);
    }

    let startRange = ((this.page + 1) * this.pageSize) - (this.pageSize - 1);
    let endRange = ((this.page + 1) * this.pageSize);
    if (endRange > this.totalElements) {
      endRange = this.totalElements;
    }

    let oPageableResponseJson = {
      isFirst: this.isFirst,
      isLast: this.isLast,
      page: this.page,
      totalElements: this.totalElements,
      totalPages: this.totalPages,
      prevPage: this.prevPage,
      nextPage: this.nextPage,
      lastPage: this.lastPage,
      startRange: startRange,
      endRange: endRange,
      pages: this.getPages(),
      sortField: this.sortField,
      sortDirection: this.sortDirection
    }

    return oPageableResponseJson;

  }

  getPages() {

    let aPages = [];

    let iStart;
    let iEnd;

    if (this.totalPages > this.pageableDisplayNumber) {

      let iPadding = Math.floor(this.pageableDisplayNumber / 2);

      iStart = (this.page - iPadding);
      iEnd = (this.page + iPadding);

      if (iStart < 0) {
        while (iStart < 0) {
          iStart += 1;
          iEnd += 1;
        }
      }

      if (iEnd > this.totalPages) {
        while (iEnd > this.totalPages) {
          iStart -= 1;
          iEnd -= 1;
        }
      }

    } else {
      iStart = 0;
      iEnd = this.totalPages;
    }

    for (let i = iStart; i < iEnd; i++) {

      var oPage = {
        label: i + 1,
        current: false,
        page: i
      };

      if (i == this.page) {
        oPage.current = true;
      }
      aPages.push(oPage);
    }

    return aPages;
  }

}
