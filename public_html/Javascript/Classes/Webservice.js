export class WebserviceRequest {

  constructor(sType, oData, sDataType, sUri, bProcessData) {
    this.type = sType;
    this.data = oData;
    this.dataType = sDataType;
    this.uri = sUri;
    this.processData = bProcessData;
  }

  makeRequest(fCallback) {

    return Promise.resolve(
      $.ajax({
        type: this.type,
        processData: this.processData,
        headers: this.headers,
        data: this.data,
        dataType: this.dataType,
        contentType: 'application/json',
        url: WeatherStation.Config.ROOT_WEBSERVICE_URL + this.uri,

        complete: function(data) {

          if (fCallback && typeof(fCallback) === "function") {
            fCallback(data);
          }
          return data;
        }
      })
    );
  }

}
