<?php

class RecordsController {

  /**
   * Define specific pages css/js resources
   */
  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'Resources/CSS/home.css',
    ]
  );

  /**
   * Produce the view
   */
  public function searchable() {

    $$pageContent['pageTitle'] = "Station Records";
    $pageContent['pageResources'] = $this->pageResources;

    if ( !isset($_GET["id"]) || !$_GET["id"]) {
      redirect(BASE_URL);
      return;
    }

    if ( !isset($_GET["siteId"]) || !$_GET["siteId"]) {
      redirect(BASE_URL);
      return;
    }

    $pageContent['stationId'] = $_GET["id"];
    $pageContent['siteId'] = $_GET["siteId"];

    if ( isset($_GET["station"]) || $_GET["station"]) {
      $pageContent['stationName'] = $_GET["station"];
    } else {
      $pageContent['stationName'] = "Station ".$_GET["id"];
    }

    if ( isset($_GET["site"]) || $_GET["site"]) {
      $pageContent['siteName'] = $_GET["site"];
    } else {
      $pageContent['siteName'] = "Site ".$_GET["siteId"];
    }

    $pageContent['loader'] = getLoadingImage();

    require_once( VIEWS_PATH."/Records.view.php" );
  }

}
