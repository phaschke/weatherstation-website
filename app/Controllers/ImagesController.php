<?php

class ImagesController {

  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'Resources/CSS/images.css',
    ]
  );

  public function bystation() {

    $pageContent['pageTitle'] = "Station Images";
    $pageContent['pageResources'] = $this->pageResources;

    if ( !isset($_GET["id"]) || !$_GET["id"]) {
      redirect(BASE_URL);
      return;
    }

    $pageContent['stationId'] = $_GET["id"];
    $pageContent['locationId'] = -1;

    $pageContent['loader'] = getLoadingImage();

    require_once( VIEWS_PATH."/Images.view.php" );
  }

  public function bylocation() {

    $pageContent['pageTitle'] = "Station Images";
    $pageContent['pageResources'] = $this->pageResources;

    if ( !isset($_GET["id"]) || !$_GET["id"]) {
      redirect(BASE_URL);
      return;
    }

    $pageContent['stationId'] = -1;
    $pageContent['locationId'] = $_GET["id"];

    $pageContent['loader'] = getLoadingImage();

    require_once( VIEWS_PATH."/Images.view.php" );
  }

  public function image() {

    $pageContent['pageTitle'] = "View Image";
    $pageContent['pageResources'] = $this->pageResources;

    if ( !isset($_GET["id"]) || !$_GET["id"]) {
      redirect(BASE_URL);
      return;
    }

    $pageContent['imageId'] = $_GET["id"];

    $pageContent['loader'] = getLoadingImage();

    require_once( VIEWS_PATH."/Image.view.php" );
  }

  public function search() {

    $pageContent['pageTitle'] = "Search Images";
    $pageContent['pageResources'] = $this->pageResources;

    if ( !isset($_GET["id"]) || !$_GET["id"]) {
      redirect(BASE_URL);
      return;
    }
    if ( !isset($_GET["siteId"]) || !$_GET["siteId"]) {
      redirect(BASE_URL);
      return;
    }

    $pageContent['locationId'] = $_GET["id"];
    $pageContent['camId'] = $_GET["siteId"];

    $pageContent['loader'] = getLoadingImage();

    require_once( VIEWS_PATH."/ImageSearch.view.php" );
  }

}
