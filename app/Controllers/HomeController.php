<?php

class HomeController {

  /**
   * Define specific pages css/js resources
   */
  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'Resources/CSS/home.css',
    ]
  );

  /**
   * Produce the view
   */
  public function show() {

    $pageContent['pageTitle'] = "Weather Stations";
    $pageContent['pageResources'] = $this->pageResources;

    $pageContent['loader'] = getLoadingImage();

    require_once( VIEWS_PATH."/Home.view.php" );
  }

}
