<?php

class ErrorController {

  public function not_found() {
    // Do something here on not found error
  }

  public function not_implemented() {
    // Do something here on not implemented error
  }

  public function permission_denied() {
    // Do something here on permission denied error
  }

}
