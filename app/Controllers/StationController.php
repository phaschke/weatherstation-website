<?php
class StationController {

  /**
   * Define specific pages css/js resources
   */
  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'Resources/CSS/station.css',
    ]
  );

  /**
   * Produce the view
   */
  public function show() {

    $pageContent['pageTitle'] = "Weather Station";
    $pageContent['pageResources'] = $this->pageResources;

    if ( !isset($_GET["id"]) || !$_GET["id"]) {
      redirect(BASE_URL);
      return;
    }
    $pageContent['stationId'] = $_GET["id"];

    $pageContent['activeSite'] = "";
    if (isset($_GET["activesite"]) && $_GET["activesite"]) {
      $pageContent['activeSite'] = $_GET["activesite"];
    }


    $pageContent['loader'] = getLoadingImage();

    require_once( VIEWS_PATH."/Station.view.php" );
  }


}
