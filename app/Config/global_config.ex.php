<?php

require_once __DIR__."/../Util/Util.php";

define("BASE_PATH", __DIR__."/..");
define("BASE_URL", "xxx");
define("DEFAULT_PAGE_URL", "dashboard/show");

//======================================================================
// ROUTING
//======================================================================
  define("ROUTES_PATH", __DIR__."/../Router");
  define("ROUTES", ROUTES_PATH."/Routes.php");


//======================================================================
// CONTROLLERS
//======================================================================
  define("CONTROLLERS_PATH", __DIR__."/../Controllers");


//======================================================================
// VIEWS
//======================================================================
  define("VIEWS_PATH", __DIR__."/../Views");
  define("SHARED_VIEWS_PATH", VIEWS_PATH."/Shared");

?>
