<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

  <div id="image-holder">
    <div>
      <img class="loading-gif text-center" src="Resources/Images/Loading/<?php echo $pageContent['loader']?>">
    </div>
  </div>

  <script>
    window.onload = function() {
      WeatherStation.Images.displayImage(<?php echo $pageContent['imageId'];?>);
    }
  </script>

</body>
