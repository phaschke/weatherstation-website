<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

  <div class="container">
    <div class="content mb-4">

      <div class="row text-center">
        <div class="col-md-3 mb-2" id="back-button">
          <a class="btn btn-outline-dark" title="Back to station" href="<?php echo BASE_URL;?>/station/show/<?php echo $pageContent['stationId'];?>"><i class="fas fa-arrow-left"></i> To Station</a>
        </div>
        <div class="col-md-6 text-center">
          <h2 class="header-text full-width">Images</h2>
        </div>
        <div class="col-md-3">
        </div>

      </div>

      <hr>

      <div id="location-holder">
        <div class="col-12 text-center">
          <img class="loading-gif" src="Resources/Images/Loading/<?php echo $pageContent['loader']?>">
        </div>
      </div>

    </div>
  </div>

  <script>
    window.onload = function() {

      WeatherStation.Images.initalizeStation(<?php echo $pageContent['stationId'];?>, <?php echo $pageContent['locationId'];?>);
    }
  </script>

<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
