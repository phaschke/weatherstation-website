<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

  <div class="container">
    <div class="content mb-4">

      <div class="row text-center">
        <div class="col-md-3 mb-2">
          <a class="btn btn-outline-dark" title="Back to all stations" href="<?php echo BASE_URL;?>/home/show"><i class="fas fa-arrow-left"></i> Stations</a>
        </div>
        <div class="col-md-6 text-center">
          <h2 id="station-name" class="header-text full-width">Weather Station <?php echo $pageContent['stationId'];?></h2>
        </div>
        <div class="col-md-3">
        </div>
      </div>

      <hr>

      <div class="row text-center">
        <div id="details-holder" class="text-center">
          <img class="loading-gif" src="Resources/Images/Loading/<?php echo $pageContent['loader']?>">
        </div>
      </div>

      <div class="row">
        <div id="site-tabs-holder" class="text-center">
        </div>
      </div>

    </div>
  </div>

  <script>
    window.onload = function() {
      WeatherStation.Stations.getStationDetails(<?php echo $pageContent['stationId'];?>, <?php echo $pageContent['activeSite'];?>);
      setInterval(function() {
        let activeTab = $('#nav-tabContent > .active').attr('id');
        WeatherStation.Stations.refreshCurrentConditions(<?php echo $pageContent['stationId'];?>, activeTab.split('-')[1]);
      }, 120000);
    }
  </script>

<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
