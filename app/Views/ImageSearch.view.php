<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

  <div class="container">
    <div class="content mb-4">

      <div class="row text-center">
        <div class="col-md-3 mb-2">
          <a class="btn btn-outline-dark" title="Back to Image Location" href="<?php echo BASE_URL;?>/images/bylocation/<?php echo $pageContent['locationId'];?>"><i class="fas fa-arrow-left"></i> To Cams</a>
        </div>
        <div class="col-md-6 text-center">
          <h2 class="header-text full-width">Image Search</h2>
        </div>
        <div class="col-md-3">
        </div>
      </div>

      <hr>

      <div class="row text-center">
        <div id="search-title-holder" class="col-md-12">
        </div>
      </div>

      <div class="row mb-2">
        <div class="col-md-3 mb-2">
          <label for="startDateTime">
            Start:
          </label>
          <input type="datetime-local" class="form-control" id="startDateTime">
          <small id="startDateTimeErrorHolder" class="text-danger">
          </small>
        </div>
        <div class="col-md-3">
          <label for="endDateTime">
            End:
          </label>
          <input type="datetime-local" class="form-control" id="endDateTime">
          <small id="endDateTimeErrorHolder" class="text-danger">
          </small>
        </div>
        <div class="col-md-3">
          <label for="dataTypeSelect">
            Image Types:
          </label>
          <select class="form-select" id="dataTypeSelect">
            <option value="ALL" selected="selected">All</option>
            <option value="ONLY_RETAINED">Only Retained</option>
            <option value="ONLY_NON_RETAINED">Only non Retained</option>
          </select>
        </div>
        <div class="col-md-3">
          <button type="button" id="image-search-btn" title="Search Cam Images" class="btn btn-outline-dark bottom" onclick="WeatherStation.Images.onSearch(<?php echo $pageContent['camId'];?>)">Search</button>
        </div>
      </div>

      <div class="row" id="image-album-holder">
      </div>

    </div>
  </div>

  <script>
    window.onload = function() {

      WeatherStation.Images.initalizeImageSearch(<?php echo $pageContent['camId'];?>);
    }
  </script>

<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
