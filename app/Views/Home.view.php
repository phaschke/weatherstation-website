<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

  <div class="container">
    <div class="content mb-4">

      <div class="row text-center">
        <h1 class="header-text full-width">Stations</h1>
      </div>

      <hr>

      <div id="station-list-holder" class="station-list text-center">
        <img class="loading-gif" src="Resources/Images/Loading/<?php echo $pageContent['loader']?>">
      </div>

      <div class="row text-center">
        <div class="col-12"><small><i><p id="page-update-time"></p></i></small></div>
        </div>
      </div>
  </div>

  <script>
    window.onload = function() {
      WeatherStation.Stations.getStationSummaries();
      document.getElementById("page-update-time").innerHTML = 'Last page update: '+ new Date();
      setInterval(function() {
        WeatherStation.Stations.getStationSummaries();
        document.getElementById("page-update-time").innerHTML = 'Last page update: '+ new Date();
      }, 120000);
    }
  </script>

<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
