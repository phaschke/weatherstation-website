<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

  <div class="container">
    <div class="content mb-4">


      <div class="row text-center">
        <div class="col-md-3 mb-2">
          <a class="btn btn-outline-dark" title="Back to station" href="<?php echo BASE_URL;?>/station/show/<?php echo $pageContent['stationId'];?>?activesite=<?php echo $pageContent['siteId'];?>"><i class="fas fa-arrow-left"></i> To Station</a>
        </div>
        <div class="col-md-6 text-center">
          <h2 id="stationName" class="header-text full-width"><?php echo $pageContent['stationName'];?></h2>
        </div>
        <div class="col-md-3">
        </div>

        <div class="row text-center">
          <div class="col-md-12">
            <p id="siteName" class=h3><?php echo $pageContent['siteName'];?></p>
          </div>
        </div>
      </div>

      <hr>

      <div class="row text-center">
        <div class="col-md-12">
          <p class=h3>Searchable Records</p>
        </div>
      </div>

      <div class="row mb-2">
        <div class="col-md-3 mb-2">
          <label for="startDateTime">
            Start:
          </label>
          <input type="datetime-local" class="form-control" id="startDateTime">
          <small id="startDateTimeErrorHolder" class="text-danger">
          </small>
        </div>
        <div class="col-md-3">
          <label for="endDateTime">
            End:
          </label>
          <input type="datetime-local" class="form-control" id="endDateTime">
          <small id="endDateTimeErrorHolder" class="text-danger">
          </small>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="float-md-start">
            <button type="button" title="Search Records" class="btn btn-outline-dark" onclick="WeatherStation.Tables.onSearch(<?php echo $pageContent['stationId'];?>, <?php echo $pageContent['siteId'];?>)">Search</button>
          </div>
          <div class="float-end">
            <button type="button" title="Download records" class="btn btn-outline-dark" onclick="WeatherStation.Util.openModal('sm', WeatherStation.Tables.openDataDownloadModal(<?php echo $pageContent['stationId'];?>, <?php echo $pageContent['siteId'];?>))"><i class="fas fa-download"></i></button>
          </div>
          <div class="float-end me-2">
            <button type="button" title="Show or hide columns" class="btn btn-outline-dark" onclick="WeatherStation.Util.openModal('sm', WeatherStation.Tables.openColumnVisibilityToggle(<?php echo $pageContent['stationId'];?>, <?php echo $pageContent['siteId'];?>))"><i class="fas fa-eye"></i></button>
          </div>
          <div class="float-end me-2">
            <button type="button" title="Select data sources" class="btn btn-outline-dark" onclick="WeatherStation.Util.openModal('sm', WeatherStation.Tables.openDataSourceModal(<?php echo $pageContent['stationId'];?>, <?php echo $pageContent['siteId'];?>))"><i class="fas fa-cogs"></i></button>
          </div>
        </div>
      </div>

      <div class="row" id="record-table-holder">
      </div>

    </div>
  </div>

  <script>
    window.onload = function() {
      WeatherStation.Tables.getStationDetails('<?php echo $pageContent['stationId'];?>', '<?php echo $pageContent['siteId'];?>');
    }
  </script>

<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
