<?php

/**
 * Redirect to a given URL using the PHP header Location
 *
 * @param string $url The url to redirect to
 */
function redirect($url) {
  header("Location: ".$url);
  die();
}

function getLoadingImage() {

  $loaders = array("1.gif", "2.gif", "3.gif", "4.gif", "5.gif");

  $index = rand(0,4);

  return $loaders[$index];

}
