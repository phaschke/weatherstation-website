<?php

return array(

  'home' => ['folder' => 'Controllers',
              'controller' => 'HomeController',
              'public' => ['show'],
              'user' => [],
              'privileged' => []
            ],
  'station' => ['folder' => 'Controllers',
              'controller' => 'StationController',
              'public' => ['show'],
              'user' => [],
              'privileged' => []
            ],
  'records' => ['folder' => 'Controllers',
              'controller' => 'RecordsController',
              'public' => ['searchable'],
              'user' => [],
              'privileged' => []
            ],
  'images' => ['folder' => 'Controllers',
              'controller' => 'ImagesController',
              'public' => ['bystation', 'bylocation', 'image', 'search'],
              'user' => [],
              'privileged' => []
            ],
  'error' => ['folder' => 'Controllers',
              'controller' => 'ErrorController',
              'public' => ['not_found', 'not_implemented', 'permission_denied'],
              'user' => [],
              'privileged' => []
            ]
  );
